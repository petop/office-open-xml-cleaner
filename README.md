# Office Open XML cleaner

Author: [Peter Peto](http://petopeter.hu)


PHP Office Open XML cleaner/normalizer

## Tech

PHP based
Tag structure uses a projects to work properly:
* [Tag builder and parser](https://bitbucket.org/petop/tag-builder-and-parser) - PHP based HTML and XML tag builder and parser
* [Tag structure builder and parser](https://bitbucket.org/petop/tag-sturcture-builder-and-parser) - PHP based HTML and XML tag structure builder and parser

## Installation

Tag builder and parser requires [PHP](https://www.php.net/) v5.3.0+ to run.

You need to add this lines into your composer.json:
```sh
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/petop/office-open-xml-cleaner.git"
    }
]
"require": {
    "petop/office-open-xml-cleaner": "dev-master"
},
```

To install the defined dependencies for your project, run the `install` command.

```sh
$ php composer.phar update
$ php composer.phar install
```

## Usage

### Parse and clear xml string
```php
$xml = new TagStructure();
$xml->parseTagString('<w:p w14:paraId="48844C7C" w14:textId="3AC40895" w:rsidR="00091799" w:rsidRPr="00D92D08" w:rsidRDefault="003F425E" w:rsidP="00D77493"></w:p>');
$xmlCleaner = new OfficeOpenXmlCleaner();
$xml = $xmlCleaner->clean($xml);
```

### Parse and clear xml file
```php
$path = './example.xml';
$xml = new TagStructure();
$xml->parseTagFile($path);
$xmlCleaner = new OfficeOpenXmlCleaner();
$xml = $xmlCleaner->clean($xml);
```

### Get cleared structure like string
```php
$xmlCleaner = new OfficeOpenXmlCleaner();
$xml = $xmlCleaner->clean($tagStructure);
$xml->__toString();
```
### Echo structure like string
```php
$xmlCleaner = new OfficeOpenXmlCleaner();
echo $xmlCleaner->clean($tagStructure);
```

## Licence Information
CC-BY-NC-SA
- ## You are free to:
  - **Share** — copy and redistribute the material in any medium or format
  - **Adapt** — remix, transform, and build upon the material
  > The licensor cannot revoke these freedoms as long as you follow the license terms.

- ## Under the following terms:
  - **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. 
  - **NonCommercial** — You may not use the material for commercial purposes.
  - **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  - **No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
  > Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.
You can also: