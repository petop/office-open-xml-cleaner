<?php
declare(strict_types = 1);

use \PHPUnit\Framework\TestCase;
use PeterPeto\OfficeOpenXmlCleaner;
use PeterPeto\TagStructure;

class OfficeOpenXmlCleanerTest extends TestCase
{
    public function testOddTagClean()
    {
        $xml = new TagStructure();
        $xml->parseTagString('<w:p/>');
        $xmlCleaner = new OfficeOpenXmlCleaner();
        $this->assertEquals($xmlCleaner->clean($xml)->__toString(), $xml->__toString());
    }

    public function testEmptyEvenTagClean()
    {
        $xml = new TagStructure();
        $xml->parseTagString('<w:t></w:t>');
        $xmlCleaner = new OfficeOpenXmlCleaner();
        $this->assertEquals($xmlCleaner->clean($xml)->__toString(), $xml->__toString());
    }

    public function testEmptyEvenTagWithAttributeClean()
    {
        $xml = new TagStructure();
        $xml->parseTagString('<w:p w14:paraId="48844C7C"></w:p>');
        $xmlCleaner = new OfficeOpenXmlCleaner();
        $this->assertEquals($xmlCleaner->clean($xml)->__toString(), $xml->__toString());
    }

    public function testEmptyEvenTagWithAttributesClean()
    {
        $xml = new TagStructure();
        $xml->parseTagString('<w:p w14:paraId="48844C7C" w14:textId="3AC40895" w:rsidR="00091799" w:rsidRPr="00D92D08" w:rsidRDefault="003F425E" w:rsidP="00D77493"></w:p>');
        $xmlCleaner = new OfficeOpenXmlCleaner();
        $this->assertEquals($xmlCleaner->clean($xml)->__toString(), $xml->__toString());
    }

    public function testNonEmptyEvenTagClean()
    {
        $string = '<w:t>attack</w:t>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $this->assertInstanceOf("PeterPeto\TagStructure", $newXml);
        $this->assertEquals($newXml->__toString(), $string);
    }

    public function testNonEmptyEvenCleanableTagClean()
    {
        $string = '<w:r w:rsidR="00686ACD" w:rsidRPr="0076582F"><w:t xml:space="preserve">attack</w:t></w:r>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $this->assertEquals($newXml->__toString(), $string);
    }

    public function testNonEmptyEvenTagWithAttributeClean()
    {
        $string = '<div><p attribute="black">Content</p></div>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $this->assertEquals($newXml, $string);
    }

    public function testOne_CleanableTagClean()
    {
        $string = '<w:p><w:r><w:lastRenderedPageBreak/><w:t>a</w:t></w:r><w:r w:rsidR="00686ACD" w:rsidRPr="0076582F"><w:t xml:space="preserve">ttack </w:t></w:r></w:p>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $assert = '<w:p><w:r><w:lastRenderedPageBreak/></w:r><w:r w:rsidR="00686ACD" w:rsidRPr="0076582F"><w:t xml:space="preserve">attack </w:t></w:r></w:p>';
        $this->assertEquals($newXml->__toString(), $assert);
    }

    public function testTwo_CleanableTagClean()
    {
        $string = '<w:p w14:paraId="6648644C" w14:textId="41240025" w:rsidR="006311FE" w:rsidRPr="006311FE" w:rsidRDefault="003F425E" w:rsidP="00D77493"><w:pPr><w:pStyle w:val="Cmsor2"/></w:pPr><w:bookmarkStart w:id="17" w:name="_Use(Place)_[int_slotnum]"/><w:bookmarkStart w:id="18" w:name="_Attack(Destroy)_[int_slotnum]"/><w:bookmarkStart w:id="19" w:name="_Toc481387910"/><w:bookmarkEnd w:id="17"/><w:bookmarkEnd w:id="18"/><w:r><w:lastRenderedPageBreak/><w:t>a</w:t></w:r><w:r w:rsidR="00686ACD" w:rsidRPr="0076582F"><w:t xml:space="preserve">ttack </w:t></w:r><w:r w:rsidR="006311FE" w:rsidRPr="0076582F"><w:t>[</w:t></w:r><w:r w:rsidR="006311FE"><w:t>string direction</w:t></w:r><w:r w:rsidR="006311FE" w:rsidRPr="0076582F"><w:t>]</w:t></w:r><w:bookmarkEnd w:id="19"/></w:p>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $assert = '<w:p w14:paraId="6648644C" w14:textId="41240025" w:rsidR="006311FE" w:rsidRPr="006311FE" w:rsidRDefault="003F425E" w:rsidP="00D77493"><w:pPr><w:pStyle w:val="Cmsor2"/></w:pPr><w:bookmarkStart w:id="17" w:name="_Use(Place)_[int_slotnum]"/><w:bookmarkStart w:id="18" w:name="_Attack(Destroy)_[int_slotnum]"/><w:bookmarkStart w:id="19" w:name="_Toc481387910"/><w:bookmarkEnd w:id="17"/><w:bookmarkEnd w:id="18"/><w:r><w:lastRenderedPageBreak/></w:r><w:r w:rsidR="006311FE" w:rsidRPr="0076582F"><w:t>attack [string direction]</w:t></w:r><w:bookmarkEnd w:id="19"/></w:p>';
        $this->assertEquals($newXml->__toString(), $assert);
    }

    public function testThree_CleanableTagClean()
    {
        $string = '<w:tr w:rsidR="000725ED" w14:paraId="271AF4F5" w14:textId="7994FE15" w:rsidTr="00A310CB"><w:tc><w:tcPr><w:tcW w:w="1975" w:type="dxa"/></w:tcPr><w:p w14:paraId="2A9D7C90" w14:textId="534DFEFE" w:rsidR="00E21D6B" w:rsidRDefault="008A36CE" w:rsidP="00FD364B"><w:r><w:t>[s</w:t></w:r><w:r w:rsidR="000725ED"><w:t xml:space="preserve">tring </w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidR="00F83F56"><w:t>block</w:t></w:r><w:r w:rsidR="00457BB1"><w:t>N</w:t></w:r><w:r w:rsidR="000725ED"><w:t>ame</w:t></w:r><w:proofErr w:type="spellEnd"/><w:r><w:t>]</w:t></w:r></w:p></w:tc></w:tr>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $assert = '<w:tr w:rsidR="000725ED" w14:paraId="271AF4F5" w14:textId="7994FE15" w:rsidTr="00A310CB"><w:tc><w:tcPr><w:tcW w:w="1975" w:type="dxa"/></w:tcPr><w:p w14:paraId="2A9D7C90" w14:textId="534DFEFE" w:rsidR="00E21D6B" w:rsidRDefault="008A36CE" w:rsidP="00FD364B"><w:r><w:t>[string blockName]</w:t></w:r></w:p></w:tc></w:tr>';
        $this->assertEquals($newXml->__toString(), $assert);
    }

    public function testHyperlinkClean()
    {
        $string = '<w:tc><w:tcPr><w:tcW w:w="2803" w:type="dxa"/></w:tcPr><w:p w14:paraId="0CEA3927" w14:textId="2AAC9268" w:rsidR="000725ED" w:rsidRDefault="00754F52" w:rsidP="009E7311"><w:hyperlink w:anchor="_Use(Place)_[int_slotnum]" w:history="1"><w:r w:rsidR="002D59E4"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>	p</w:t></w:r><w:r w:rsidR="000725ED"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>	lace</w:t></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t xml:space="preserve">	 [int </w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>		slot</w:t></w:r><w:r w:rsidR="00A37763"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>N</w:t></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>	um</w:t></w:r><w:proofErr w:type="spellEnd"/><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>	]</w:t></w:r><w:r w:rsidR="00A37763"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:br/></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>	[string direction]</w:t></w:r></w:hyperlink></w:p></w:tc>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $assert = '<w:tc><w:tcPr><w:tcW w:w="2803" w:type="dxa"/></w:tcPr><w:p w14:paraId="0CEA3927" w14:textId="2AAC9268" w:rsidR="000725ED" w:rsidRDefault="00754F52" w:rsidP="009E7311"><w:hyperlink w:anchor="_Use(Place)_[int_slotnum]" w:history="1"><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>place [int slotNum]</w:t></w:r><w:r w:rsidR="00A37763"><w:br/></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hyperlink"/></w:rPr><w:t>[string direction]</w:t></w:r></w:hyperlink></w:p></w:tc>';
        $this->assertEquals($newXml->__toString(), $assert);
    }

    public function testHiperhivatkozsClean()
    {
        $string = '<w:tc><w:tcPr><w:tcW w:w="2803" w:type="dxa"/></w:tcPr><w:p w14:paraId="0CEA3927" w14:textId="2AAC9268" w:rsidR="000725ED" w:rsidRDefault="00754F52" w:rsidP="009E7311"><w:hyperlink w:anchor="_Use(Place)_[int_slotnum]" w:history="1"><w:r w:rsidR="002D59E4"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>	p</w:t></w:r><w:r w:rsidR="000725ED"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>	lace</w:t></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t xml:space="preserve">	 [int </w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>		slot</w:t></w:r><w:r w:rsidR="00A37763"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>N</w:t></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>	um</w:t></w:r><w:proofErr w:type="spellEnd"/><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>	]</w:t></w:r><w:r w:rsidR="00A37763"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:br/></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>	[string direction]</w:t></w:r></w:hyperlink></w:p></w:tc>';
        $xml = new TagStructure();
        $xml->parseTagString($string);

        $xmlCleaner = new OfficeOpenXmlCleaner();
        $newXml = $xmlCleaner->clean($xml);

        $assert = '<w:tc><w:tcPr><w:tcW w:w="2803" w:type="dxa"/></w:tcPr><w:p w14:paraId="0CEA3927" w14:textId="2AAC9268" w:rsidR="000725ED" w:rsidRDefault="00754F52" w:rsidP="009E7311"><w:hyperlink w:anchor="_Use(Place)_[int_slotnum]" w:history="1"><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>place [int slotNum]</w:t></w:r><w:r w:rsidR="00A37763"><w:br/></w:r><w:r w:rsidR="000725ED" w:rsidRPr="00D71454"><w:rPr><w:rStyle w:val="Hiperhivatkozs"/></w:rPr><w:t>[string direction]</w:t></w:r></w:hyperlink></w:p></w:tc>';
        $this->assertEquals($newXml->__toString(), $assert);
    }
}
