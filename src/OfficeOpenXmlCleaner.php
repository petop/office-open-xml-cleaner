<?php
namespace PeterPeto;
use PeterPeto\Tag;
use PeterPeto\TagStructure;

class OfficeOpenXmlCleaner
{
    public function clean(TagStructure $xml)
    {   
        $structure = $xml->getStructure();
        $newStructure = $this->cleanProcess($structure);
        $newXml = new TagStructure();
        $newXml->parseTagString( $xml->getHeader() . $newStructure->__toString());
        
        return $newXml;
    }

    private function cleanProcess(Tag $tag)
    {
        $contents = $this->removeUnnecessaryTags($tag->getContent());
        if(count($contents) == 0 || (count($contents) == 1 && ($contents[0] instanceof Tag) == FALSE)) {
            return $tag;
        }

        $contents = count($contents) == 1 ? [$this->cleanProcess($contents[0])] : $this->cleanMultipleContents($contents);
        $contents = $this->removeUnnecessaryTags($contents);

        $tag->clearContent();
        foreach($contents as $data)
        {
            if($data instanceof Tag && (count($data->getContent()) > 0 || $data->isOdd()))
            {
                $tag->addTag($data);
            }
            else if(($data instanceof Tag) == FALSE)
            {
                $tag->addContent($data);
            }
        }
        return $tag;
    }

    private function removeUnnecessaryTags($contents)
    {
        foreach($contents as $index => $data)
        {
            $case1 = $this->isWProofErrTag($data);
            $case2 = $this->isWRTag($data) && count($data->getContent()) == 1 && $this->isWrPrWithHyperlink($data->getContent()[0]);

            if($case1 || $case2)
            {
                unset($contents[$index]);
            }
        }

        $contents = array_values($contents);

        if(count($contents) == 2 && $this->isWrPrWithHyperlink($contents[0]) && $this->isWBrTag($contents[1]))
        {
            unset($contents[0]);
        }

        if(count($contents) == 2 && $this->isWBrTag($contents[0]) && $this->isWrPrWithHyperlink($contents[1]))
        {
            unset($contents[1]);
        }

        return array_values($contents);
    }

    private function cleanMultipleContents($contents)
    {
        $i = 0;
        while($i < count($contents) - 1)
        {
            if(($contents[$i] instanceof Tag) == FALSE)
            {
                $i++;
                continue;
            }

            if($this->cleanable($contents[$i], $contents[$i + 1]))
            {
                $charIndex = count($contents[$i]->getContent()) - 1;
                $subtag = count($contents[$i + 1]->getContent()) == 1 ? $contents[$i + 1]->getContent()[0] : $contents[$i + 1]->getContent()[1];
                $string = $contents[$i]->getContent()[$charIndex]->getContent()[0] . $subtag->getContent()[0];
                $subtag->clearContent();
                $subtag->addContent($string);

                if(count($contents[$i]->getContent()) > 1)
                {
                    $tagTmp = $contents[$i]->getContent()[0];
                    $contents[$i]->clearContent();
                    $contents[$i]->addTag($tagTmp);
                }
                else
                {
                    $contents[$i]->clearContent();
                }
            }
            elseif(count($contents[$i]->getContent()) > 0)
            {
                $contents[$i] = $this->cleanProcess($contents[$i]);
            }
            $i++;
        }
        $contents[$i] = $this->cleanProcess($contents[$i]);

        return $contents;
    }


    private function cleanable(Tag $tag, Tag $nextTag)
    {
        if($this->isWRTag($tag) == FALSE)
        {
            return false;
        }
        
        $contents = $tag->getContent();
        $case1 = count($contents) == 1 && $this->isWtTagWithString($contents[0]);

        $case2Condition1 = count($contents) == 2;
        $case2Condition2 = $case2Condition1 && ($contents[0] instanceof Tag) && in_array($contents[0]->getName(), ['w:rPr', $this->getLastRenderedPageBreakTag()->getName()]);
        $case2 = $case2Condition2 && $this->isWtTagWithString($contents[1]);

        $nextTagContent = $nextTag->getContent();
        $nextTagCase1 = count($nextTagContent) == 1 && $this->isWtTagWithString($nextTagContent[0]);
        $nextTagCase2 = count($nextTagContent) == 2 && $this->isWrPrWithHyperlink($nextTagContent[0]) && $this->isWtTagWithString($nextTagContent[1]);

        return ($case1 || $case2) && ($nextTagCase1 || $nextTagCase2);
    }

    private function getLastRenderedPageBreakTag()
    {
        return new Tag('<w:lastRenderedPageBreak/>');
    }

    private function isWrPrWithHyperlink($tag)
    {
        $condition1 = ($tag instanceof Tag) && $tag->getName() == 'w:rPr';
        $condition2 = $condition1 && count($tag->getContent()) == 1 && ($tag->getContent()[0] instanceof Tag);
        $condition3 = $condition2 && $tag->getContent()[0]->getName() == 'w:rStyle';
        $subAttributes = $condition3 ? $tag->getContent()[0]->getAttributes() : [];
        return $condition3 && count($subAttributes) == 1 && isset($subAttributes['w:val']) && isset($subAttributes['w:val'][0]) && in_array($subAttributes['w:val'][0], ['Hyperlink', 'Hiperhivatkozs']);
    }

    private function isWtTagWithString($tag)
    {
        $condition1 = ($tag instanceof Tag) && $tag->getName() == 'w:t';
        $condition2 = $condition1 && count($tag->getContent()) == 1;
        return $condition2 && gettype($tag->getContent()[0]) == 'string';
    }

    private function isWProofErrTag($content)
    {
        return ($content instanceof Tag && $content->getName() == 'w:proofErr');
    }

    private function isWRTag($content)
    {
        return $content instanceof Tag && $content->getName() == 'w:r';
    }

    private function isWBrTag($content)
    {
        return $content instanceof Tag && $content->getName() == 'w:br';
    }
}
